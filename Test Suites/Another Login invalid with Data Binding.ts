<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Another Login invalid with Data Binding</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>7f602960-20ad-4f3f-9c64-c8320989b1a2</testSuiteGuid>
   <testCaseLink>
      <guid>87541741-4de0-4d54-b4a2-b514c6fc141b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC-03 login invalid user data binding</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>84278e8d-a8b9-45e5-a91a-f75cee3ee215</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/loginInvalid</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>84278e8d-a8b9-45e5-a91a-f75cee3ee215</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>31e19c49-ceaf-42b5-a509-143975eb8d71</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>84278e8d-a8b9-45e5-a91a-f75cee3ee215</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>e60bc95a-ad8b-47e0-9f86-9e070d6c9897</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
