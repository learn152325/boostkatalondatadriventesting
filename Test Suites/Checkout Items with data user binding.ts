<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Checkout Items with data user binding</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>76765019-e9b7-4247-aa17-c8310c82ab6e</testSuiteGuid>
   <testCaseLink>
      <guid>b6f3cf5d-f3e6-4b7e-98e9-e892b5d0a3dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC-05 checkout items with data binding</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>ffea9e23-f176-443a-9d4e-016d397963a5</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/userCheckout</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>ffea9e23-f176-443a-9d4e-016d397963a5</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>firstname</value>
         <variableId>7f8dd5cf-c29b-4b6b-8934-7c97f2a27f9a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ffea9e23-f176-443a-9d4e-016d397963a5</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>lastname</value>
         <variableId>f2b6faed-9a1a-433e-afb5-034ca1a5296f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ffea9e23-f176-443a-9d4e-016d397963a5</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>postalcode</value>
         <variableId>f344d4ff-80b6-43fa-aedc-c07a820bce54</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
